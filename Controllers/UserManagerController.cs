﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Linq;
using UserManagerJsonProcessor.Domain;
using Iper.Common.EntityFramework;
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace UserManagerJsonProcessor.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserManagerController : ControllerBase
	{
		private readonly IUserManager _userManager;
		private readonly IConfiguration _configuration;
		public UserManagerController(IUserManager userManager, IConfiguration configuration)
		{
			_userManager = userManager;
			_configuration = configuration;
		}

		private readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
		{
			TypeNameHandling = TypeNameHandling.Auto,
			Formatting = Formatting.Indented
		};

		private IEnumerable<User> GetFormattedUserManagerData(IEnumerable<UserManager> userInfo)
		{
			return userInfo
				.GroupBy(prop => new { prop.Id })
				.Select(prop => new User
				{
					userId = prop.Key.Id,
					name = prop.First().Name,
					email = prop.First().EmailId,
					accountRoleDefinitionIds = prop.GroupBy(p => p.RoleId).Select(p => p.First().RoleId).ToList()
				}).ToList();
		}

		private string MapRoleName(int roleId)
		{
			switch (roleId)
			{
				case 3:
				case 1:
					return "Admin";
				case 7:
					return "Editor";
				case 8:
					return "User";
				default:
					return string.Empty;
			}
		}

		private IEnumerable<Account> GetCompanyRoles(IEnumerable<UserManager> userInfo)
		{
			return userInfo.GroupBy(prop => new { prop.CompanyId }).Select(prop => new Account
			{
				id = prop.First().CompanyId,
				name = prop.First().CompanyName,
				roleDefinitions = prop.GroupBy(p => p.RoleId).Select(p => new RoleDefinition
				{
					id = p.First().RoleId,
					label = MapRoleName(p.First().AccountRoleId),
					perms = new List<string>(),
					isAccountAdmin = p.First().AccountRoleId == 3 || p.First().AccountRoleId == 1
				}).ToList()
			}).ToList();
			// }).Where(p => p.roleDefinitions.All(p => !p.isAccountAdmin)).ToList();
		}

		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var userInfo = await _userManager.FetchUserManagerData();
			var getCompanyRoles = GetCompanyRoles(userInfo);
			var formattedUserManagerData = GetFormattedUserManagerData(userInfo);
			var data = JsonConvert.SerializeObject(new {
				date = DateTime.Today.ToString("d"),
				env=_configuration.GetConnectionString("DefaultConnection").Contains("PP") ? "PP" : "Dev",
				users = formattedUserManagerData,
				accounts = getCompanyRoles
			}, SerializerSettings);
			byte[] bytes = Encoding.UTF8.GetBytes(data);
			MemoryStream stream = new MemoryStream(bytes);
			return File(stream, "application/json", "UserManager.json");
		}
	}
}
