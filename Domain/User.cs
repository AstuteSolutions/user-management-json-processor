﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagerJsonProcessor.Domain
{
	public class User
	{
        public int userId { get; set; }

        public string name { get; set; }

        public string email { get; set; }

        public List<int> accountRoleDefinitionIds { get; set; }

    }
}
