﻿using System.Collections.Generic;

namespace UserManagerJsonProcessor.Domain
{
	public class Account
	{
        public int id { get; set; }

        public string name { get; set; }

        public List<RoleDefinition> roleDefinitions { get; set; }

    }
}
