﻿namespace UserManagerJsonProcessor.Domain
{
	public class UserManager
	{
		public int Id { get; set; }

		public int RoleId { get; set; }

		public int CompanyId { get; set; }

		public string Name { get; set; }

		public string EmailId { get; set; }

		public string CompanyName { get; set; }

		public int AccountRoleId { get; set; }
	}
}
