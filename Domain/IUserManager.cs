﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace UserManagerJsonProcessor.Domain
{
	public interface IUserManager
	{
		Task<IEnumerable<UserManager>> FetchUserManagerData();

		Task<IEnumerable<Account>> GetAccounts();
	}
}
