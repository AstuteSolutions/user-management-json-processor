﻿using System.Collections.Generic;

namespace UserManagerJsonProcessor.Domain
{
	public class RoleDefinition
	{
		public int id { get; set; }

		public string label { get; set; }

		public List<string> perms { get; set; }

		public bool isAccountAdmin { get; set; }

	}
}
