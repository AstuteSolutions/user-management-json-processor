﻿using Iper.Common.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace UserManagerJsonProcessor.Domain
{
	public class UserManagerDomainService : IUserManager
	{
		private readonly WebValidatorContext _webValidatorContext;
		public UserManagerDomainService(WebValidatorContext context)
		{
			_webValidatorContext = context;
		}
		public async Task<IEnumerable<UserManager>> FetchUserManagerData()
		{
			int[] ids = { 1, 184, 308, 315, 321, 322, 323, 324, 338, 347, 353, 358, 370, 372, 391, 420, 1611, 1945, 2369, 2787, 3298, 4553, 4696, 4735, 4859, 5770, 6579, 9871, 10298, 11381, 12256, 12426, 14150, 14621, 15018, 15019, 15291, 16064, 17150, 17174, 17331, 17332, 18422, 20565, 21346, 22162, 22192, 22299, 23085, 23394, 23398, 23493, 23889, 23891, 24354, 24978, 26585, 27710, 27878, 27896, 28197, 28825, 28883, 29072, 29282, 29293, 29304, 29545, 29665, 29667, 29930, 30155, 30434, 30836, 30858, 30968, 30972, 31312, 31354, 31421, 31912, 32185, 32230, 32236, 32405, 32441, 32551, 32589, 32683, 32765, 32925, 33002, 33050, 33123, 33200, 33437, 33465, 33489, 33500, 33521, 33578, 33911, 34101, 34114, 34177, 34217, 34220, 34224, 34384, 34523, 34568, 34626, 34667, 34746, 34855, 34961, 35007, 35144, 35167, 35241, 35243, 35282, 35298, 35374, 35379, 35402, 35407, 35410, 35411, 35419, 35423, 35425, 35433, 35439, 35441, 35442, 35445, 35446, 35448, 35450, 35451, 35454, 35455, 35457, 35458, 35459, 35460, 35465, 35466, 35467, 35469, 35470, 35471, 35472, 35474, 35476, 35477, 35479, 35480, 35485, 35488, 35489, 35490, 35491, 35492, 35495, 35497, 35498, 35500, 35502, 35504, 35505, 35507, 35508, 35509, 35511, 35512, 35513, 35514 };
			return await (from ru in _webValidatorContext.RolesUsers
						  join rur in _webValidatorContext.UserRoles on ru.Id equals rur.UserId
						  join ucb in _webValidatorContext.UserCompanyBelongTos on rur.CompanyId equals ucb.CompanyId
						  where ids.Contains(ucb.CompanyId)
						  join u in _webValidatorContext.Users on ru.UserId equals u.UserId
						  join ci in _webValidatorContext.CompanyInfoes on rur.CompanyId equals ci.CompanyId
						  where u.UserEmail.IndexOf("INVALID") < 0 && u.UserEmail.IndexOf("_deleted") < 0 && u.UserEmail.IndexOf("deleted") < 0
						  /*join sa in _webValidatorContext.SurveyAccounts on ci.CompanyId equals sa.CompanyId */
						  select new UserManager
						  {
								Id = rur.UserId,
								RoleId = Int32.Parse(ucb.CompanyId + "" + rur.RoleId),
								CompanyId = ucb.CompanyId,
								Name = ru.FirstName +' '+ ru.LastName,
								EmailId = u.UserEmail,
								CompanyName = ci.CompanyName,
							    AccountRoleId = rur != null ? rur.RoleId : 0
						   }).ToListAsync();
		}

		public async Task<IEnumerable<Account>> GetAccounts()
		{
			return await (from ci in _webValidatorContext.CompanyInfoes
						  select new Account
						  {
							id = ci.CompanyId,
							name = ci.CompanyName,
							roleDefinitions = new List<RoleDefinition>()
						  }).ToListAsync();
		}
	}
}
